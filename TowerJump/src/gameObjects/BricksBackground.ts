/// <reference path = "../../lib/phaser.d.ts"/>

module TowerJump {
    export class BricksBackground {
        game: Phaser.Game;
        backgrounds: Array<Phaser.Sprite>;

        velocityY: number;

        constructor(game: Phaser.Game, velocityY: number) {
            this.game = game;
            this.velocityY = velocityY;
            this.backgrounds = [];
            for (let i = 0; i < 2; i++) {
                let bg = this.game.add.sprite(0,0, "bgTower");
                bg.y = i*bg.height;
                bg.scale.set(1.4);
                bg.sendToBack();
                bg.tint = 0x737373;
                this.backgrounds.push(bg);
            }
        }

        update() {
            this.backgrounds.forEach(bg=>{
                bg.y+=this.velocityY;
                if(bg.y>=this.game.height){
                    bg.y = -bg.height;
                }
            });

        }
    }
}