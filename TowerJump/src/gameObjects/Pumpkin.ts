/// <reference path = "../../lib/phaser.d.ts"/>

module TowerJump {

    export enum State {IDLE, MOVE, JUMP};

    export class Pumpkin {
        game: Phaser.Game;
        currentState: State;
        states: Phaser.Sprite;
        leftKey: Phaser.Key;
        rightKey: Phaser.Key;
        spaceKey: Phaser.Key;

        constructor(game: Phaser.Game) {
            this.game = game;
            this.currentState = State.IDLE;

            //TODO: Remove this!

            this.states = this.game.add.sprite(400, 0, "pumpkin");
            this.states.scale.set(0.15, 0.15);
            this.states.anchor.set(0.5, 0.5);
            // this.states.animations.add("Fly",Phaser.Animation.generateFrameNames("Fly (",));
            this.states.animations.add("Run", Phaser.Animation.generateFrameNames("Run (", 1, 8, ")"), 22, true);
            this.states.animations.add("Idle", Phaser.Animation.generateFrameNames("Idle (", 1, 8, ")"), 22, true);
            this.states.animations.add("Jump", Phaser.Animation.generateFrameNames("Jump (", 1, 8, ")"), 22, false);
            this.states.animations.play("Idle");

            this.game.physics.enable(this.states, Phaser.Physics.ARCADE);
            this.states.body.gravity.set(0, 5000);

            this.leftKey = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);

            this.rightKey = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
            this.spaceKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

            this.leftKey.onUp.add(() => {
                this.stopRunning();
            });

            this.rightKey.onUp.add(() => {
                this.stopRunning();
            });
            this.spaceKey.onDown.add(() => {
                this.jump();
            });
        }

        update() {
            if (this.leftKey.isDown) {
                this.run(-1);
            }
            if (this.rightKey.isDown) {
                this.run(1);
            }
        }

        run(direction: number) {
            if (direction == -1) {
                this.states.scale.set(-0.15, 0.15);
            }
            else {
                this.states.scale.set(0.15, 0.15);
            }

            this.states.body.velocity.x = direction * 600;
            if (this.currentState !== State.JUMP) {
                this.currentState = State.MOVE;
                this.states.animations.play("Run");
            }
        }

        stopRunning() {
            if (this.currentState !== State.JUMP) {
                this.currentState = State.IDLE;
                this.states.animations.play("Idle");
            }

            this.states.body.velocity.x = 0;

        }

        jump() {
            if (this.currentState !== State.JUMP) {
                this.states.animations.play("Jump");
                this.currentState = State.JUMP;
                this.states.body.velocity.y = -1800;
            }
        }

        resetOnGround() {
            if (this.currentState === State.JUMP) {

                this.currentState = State.IDLE;
                this.states.animations.play("Idle");
            }
        }
    }
}
