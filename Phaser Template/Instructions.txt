1. Download NODE from here https://nodejs.org/en/
2. Download Webstorm (you can use 1 month trial) https://www.jetbrains.com/webstorm/
3. Open this project from Webstorm
4. Got to Settings-> Languages and Frameworks
   a. Under "Node" add the path to node
   b. Under "typescript" add the path to node, check "enable typescript compiler" from tsconfig.json
5. At the bottom of Webstorm, go to typescript and find the "Compile All" button
6. Run the project through Chrome, from index.html top-right hand corner.

Included files rundown:
1. index.html is where the application starts. We load the phaser script, our own scripts and create the div container for the game
2. SampleCss.css gives some styling to the html document, but mostly, it centers the game on the screen.
3. GameName.ts creates the game.
4. The lib folder contains phaser and typescript definitions
5. src/assets -> Put your graphics, animations, sounds and so on here
6. gameObjects -> Write your own classes here
7. states -> All your game states are stored here