/// <reference path = "../../lib/phaser.d.ts"/>
///<reference path="../gameObjects/BricksBackground.ts"/>
///<reference path="../gameObjects/Platform.ts"/>
///<reference path="../gameObjects/Pumpkin.ts"/>

module TowerJump {
    export class Game extends Phaser.State {
        bricksBg: BricksBackground;
        platforms:Array<Platform>;
        initialPlatform: Platform;
        pumpkin:Pumpkin;

        create() {
            this.game.time.advancedTiming = true;
            this.game.physics.startSystem(Phaser.Physics.ARCADE);

            this.bricksBg = new BricksBackground(this.game, 0.7);
            this.platforms = [];
            this.game.time.events.loop(1000, () => {
                this.platforms.push(new Platform(this.game, 4,0));
                this.pumpkin.states.bringToTop();
            }, this);

            this.initialPlatform = new Platform(this.game, 0, 0);
            this.initialPlatform.platformsGroup.x = this.game.width*0.3;
            this.initialPlatform.platformsGroup.y = this.game.height*0.6;
            this.game.physics.enable(this.initialPlatform.platformsGroup, Phaser.Physics.ARCADE);

            this.game.time.events.add(5000, ()=>{
                this.initialPlatform.destroy();
                this.initialPlatform = null;
            });

            this.pumpkin = new Pumpkin(this.game);
            this.pumpkin.states.bringToTop();
        }

        update() {

            this.bricksBg.update();
            this.platforms.forEach(p=>{
                p.update();
            });
            this.pumpkin.update();

            this.platforms.forEach(platform=> {
                platform.platformsGroup.children.forEach((block: Phaser.Sprite) => {
                    this.game.physics.arcade.collide(this.pumpkin.states,block,()=>{
                        this.pumpkin.resetOnGround();
                    });
                });
            });

            if(this.initialPlatform) {
                this.game.physics.arcade.collide(this.pumpkin.states, this.initialPlatform.platformsGroup,()=>{
                    this.pumpkin.resetOnGround();
                });
            }
        }
        render() {
            if(this.pumpkin){
                this.game.debug.text(this.pumpkin.currentState.toString(), 20,20);
            }
        }
    }
}