/// <reference path = "../../lib/phaser.d.ts"/>

module GameName{
    export class Game extends Phaser.State{
        // ADD CLASS PROPERTIES HERE

        create(){
            // INITIALIZE GAME ELEMENTS HERE
        }

        update(){
            // THIS METHOD IS CALLED 60 TIMES A SECOND. PUT YOUR GAME LOGIC HERE
        }

        render(){
            // USED FOR DEBUGGING, e.g

        }
    }
}