/// <reference path = "lib/phaser.d.ts"/>

module TowerJump {

    class TowerJump extends Phaser.Game {

        constructor(width?:number, height?:number) {
            // Creates the game
            super(width, height, Phaser.CANVAS, 'phaser-div');
            this.state.add("Boot", Boot, false);
            this.state.add("Menu", Menu, false);
            this.state.add("Game", Game, false);

            this.state.start("Boot");
        }
    }

    window.onload = ()=> {
            new TowerJump(1024, 720);
    }
}