/// <reference path = "../../lib/phaser.d.ts"/>

module TowerJump {
    export class Boot extends Phaser.State {
        background:Phaser.Image;

        preload() {

            // MENU ASSETS
            this.game.load.image("background","src/assets/graphics/Menu/bg.png");
            this.game.load.image("starWhite","src/assets/graphics/Menu/star-white.png");
            this.game.load.image("starYellow","src/assets/graphics/Menu/star-yellow.png");
            this.game.load.image("pumpkinLogo","src/assets/graphics/Menu/pumpkin-logo.png");

            //WORM ANIMATIONS
            this.game.load.atlas("pumpkin", "src/assets/graphics/Pumpkin/pumpkin.png","src/assets/graphics/Pumpkin/pumpkin.json");

            // PLATFORMS
            this.game.load.image("bgTower","src/assets/graphics/Stage/background-tower.jpg");
            this.game.load.image("pLeft","src/assets/graphics/Stage/platform-left.png");
            this.game.load.image("pCenter","src/assets/graphics/Stage/platform-center.png");
            this.game.load.image("pRight","src/assets/graphics/Stage/platform-right.png");
        }

        create() {
            this.game.state.start("Menu");
        }
    }
}