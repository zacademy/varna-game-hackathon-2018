module TowerJump {
    export class Platform {
        game: Phaser.Game;
        platformsGroup: Phaser.Group;
        velocityY: number;
        platformScale: number;

        constructor(game: Phaser.Game, velocityY, leftWallX) {
            this.game = game;
            this.velocityY = velocityY;
            this.platformScale = 0.5;

            this.platformsGroup = this.game.add.group();
            let numberOfCenterWalls = Math.round(Math.random() * 3);
            let leftPlatform = this.game.add.sprite(0, -200, "pLeft");
            leftPlatform.scale.set(0.5,0.5);
            this.platformsGroup.add(leftPlatform);
            for (let i = 0; i < numberOfCenterWalls; i++) {
                let centerPlatform = this.game.add.sprite((i + 1) * leftPlatform.width, -200, "pCenter");
                centerPlatform.scale.set(0.5,0.5);
                this.platformsGroup.add(centerPlatform);
            }
            let rightPlatform = this.game.add.sprite((numberOfCenterWalls + 1) * leftPlatform.width, -200, "pRight");
            rightPlatform.scale.set(0.5,0.5);
            this.platformsGroup.add(rightPlatform);


            let randomX =  Math.random() * (this.game.width - (numberOfCenterWalls + 2) * leftPlatform.width);
            this.platformsGroup.x = randomX;
            this.platformsGroup.children.forEach((platform: Phaser.Sprite) => {
                this.game.physics.enable(platform, Phaser.Physics.ARCADE);
                platform.body.checkCollision.up = true;
                platform.body.checkCollision.down = false;
                platform.body.checkCollision.left = false;
                platform.body.checkCollision.right = false;
                platform.body.immovable = true;
            });
        }

        update() {
            this.platformsGroup.y += this.velocityY;
        }

        destroy() {
            this.platformsGroup.destroy(true);
        }
    }
}