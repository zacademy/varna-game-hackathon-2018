module TowerJump{
    const BORDER_WIDTH = 2;
    export class Menu extends Phaser.State{
        background: Phaser.Sprite;
        logo: Phaser.Sprite;
        startButton: Phaser.Sprite;
        startText:Phaser.Text;
        startGroup:Phaser.Group;
        bmd:Phaser.BitmapData;
        starEmitter:Phaser.Particles.Arcade.Emitter;

        create(){
            this.background = this.game.add.sprite(0,0,"background");
            this.background.scale.x = this.game.width;

            this.emitterCreate();
            this.logo = this.game.add.sprite(this.game.width/2, -100, "pumpkinLogo");
            this.game.add.tween(this.logo.position).to({y:this.game.height*0.3},1000,Phaser.Easing.Bounce.Out,true);
            this.logo.anchor.set(0.5,0.5);
            this.logo.scale.set(0.5,0.5);
            this.createPlayButton();
        }

        createPlayButton(){
            this.bmd = this.game.make.bitmapData(this.game.width*0.15+BORDER_WIDTH*2,this.game.height*0.08+BORDER_WIDTH*2,"bmdButton",true);

            this.bmd.ctx.strokeStyle = "#ffffff";
            this.bmd.ctx.fillStyle = "#000000";
            this.bmd.ctx.lineWidth = BORDER_WIDTH;
            this.bmd.ctx.lineJoin = "round";
            this.bmd.ctx.fillRect(BORDER_WIDTH/2,BORDER_WIDTH/2, this.game.width*0.15+BORDER_WIDTH,this.game.height*0.08+BORDER_WIDTH);
            this.bmd.ctx.strokeRect(BORDER_WIDTH/2,BORDER_WIDTH/2, this.game.width*0.15+BORDER_WIDTH,this.game.height*0.08+BORDER_WIDTH);

            this.startButton = this.game.add.sprite(this.game.width*0.5,this.game.height*0.7,this.game.cache.getBitmapData("bmdButton"));
            this.startButton.anchor.set(0.5,0.5);

            this.startButton.tint = 0x737373;
            this.startButton.inputEnabled = true;
            this.startButton.events.onInputOver.add(()=>{
                this.startButton.tint = 0xffffff;
            },this);

            this.startButton.events.onInputOut.add(()=>{
                this.startButton.tint = 0x737373;
            },this);

            this.startButton.events.onInputDown.add(()=>{
                this.game.state.start("Game");
            },this);

            this.startText = this.game.add.text(this.startButton.x,this.startButton.y,"Start", { font: "26px 'Rammetto One'", fill: "#737373" });
            this.startText.anchor.set(0.5,0.5);

            this.startGroup = this.game.add.group();
            this.startGroup.add(this.startButton);
            this.startGroup.add(this.startText);
            this.startGroup.alpha = 0;

            this.game.add.tween(this.startGroup).to({alpha:1}, 500, Phaser.Easing.Default, true, 1500);

        }

        emitterCreate(){
            this.starEmitter = this.game.add.emitter(0,0,200);
            this.starEmitter.width = this.game.width*0.8;
            this.starEmitter.makeParticles(["starWhite","starYellow"]);
            this.starEmitter.minParticleSpeed.set(200,200);
            this.starEmitter.maxParticleSpeed.set(700,600);

            this.starEmitter.minParticleScale = 0.3;
            this.starEmitter.maxParticleScale = 0.5;
            this.starEmitter.setRotation(30,250);

            this.starEmitter.start(false,3000,200,1000,false);
        }
    }

}